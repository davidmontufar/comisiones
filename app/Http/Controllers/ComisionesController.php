<?php

namespace App\Http\Controllers;

use App\Empleado;
use App\Venta;
use App\Comision;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;


class ComisionesController extends Controller
{
    //Valores de comision por niveles
    private $prctgComision = [1 => 3, 2 => 1.2, 3 => 0.12, 4 => 0.08, 5 => 0.06];

    /**
     * Inicio de la funcionalidad
     *
     * @return view
     */
    public function index()
    {
        //$vendedores = Empleado::with('allChildrenAccounts')->whereNull('idlider')->get()->toArray();
        //$vendedores = Empleado::with('allChildrenAccounts')->where('id',1)->get()->toArray();
        //dd($vendedores);
        return view('index');
    }

    /**
     * Invoca la pantalla de registro de ventas
     *
     * @return view venta
     */
    public function desplegarRegistrarVenta()
    {
        $vendedores = Empleado::all();
        $data = [
            'empleadosList' => $vendedores
        ];
        return view('venta', $data);
    }

    public function registrarVenta(Request $request)
    {
        dd($this->calculaComision($request->all()));
    }

    /**
     * Calcula las comisiones para el empleado y todos sus superiores dado el id y el monto de la venta
     *
     * @param integer $idVendedor
     * @param decimal $monto
     * @return array comisiones
     */
    private function calculaComision($request)
    {
        $response = [];
        //Registro de venta -- vendedor ejecutante
        $venta = new Venta;
        $venta->fecha = $request['fecha'];
        $venta->monto = $request['monto'];
        $idVendedor = $request['vendedor'];
        $venta->idempleado = $idVendedor;
        $venta->save();


        $tieneLider = true;
        $nivelBeneficiario = 1;
        $arrComisiones = [];

        while ($tieneLider) {
            //Consulta el actual vendedor desde el repositorio de la bdd
            $vendedor = Empleado::find($idVendedor);
            //Control de max niveles que comisionan
            if (!isset($this->prctgComision[$nivelBeneficiario])) {
                break;
            }

            $arrComisiones[] = [
                                'monto' => round($venta->monto*$this->prctgComision[$nivelBeneficiario]/100, 2),
                                'idventa' => $venta->id,
                                'idempleado' => $idVendedor,
                                'descripcion' => '%: '.$this->prctgComision[$nivelBeneficiario].'  Total de la venta USD: '.$venta->monto
                            ];
            
            // Si el actual nivel tiene superior
            if (is_null($vendedor->idlider)) {
                $tieneLider = false;
            } else {
                $idVendedor = $vendedor->idlider;
                $tieneLider = true;
            }

            $nivelBeneficiario += 1;
        }

        //insercion bulk de comisiones
        $arrResponse = Comision::insert($arrComisiones);
        $response['venta'] = $venta->toArray();
        $response['comisiones'] = $arrComisiones;

        if ($arrResponse) {
            $response['txt'] = 'Se registro correctamente las comisiones';
        } else {
            $response['txt'] = 'Error en el registro de las comisiones';
        }
        return $response;
    }

    /**
     * Despliega la informacion de todos los empleados: Ventas, comisiones y bonos
     *
     * @return Array
     */
    public function desplegarReporteVentComBon()
    {
        $ultimoMes = date('Y-m', strtotime('-1 month'));
        $periodo_2 = date('Y-m', strtotime('-2 month'));
        $periodo_3 = date('Y-m', strtotime('-3 month'));

        //Periodo en curso
        $fechaHoy = date('Y-m');

        //Fecha de incio de ventas periodo anterior
        $fechaInicioPeriodo = date("Y-m-d", strtotime($ultimoMes.'-01'));
        //Fecha primera quincena del mes anterior
        $fechaFinPrimeraQuincena = date("Y-m-d", strtotime($ultimoMes.'-16'));
        //Primer dia del presente mes
        $fechaFinPeriodo = date("Y-m-d", strtotime($fechaHoy.'-01'));

        //Fecha de incio de ventas periodo -2
        $fechaInicioPeriodo_2 = date("Y-m-d", strtotime($periodo_2.'-01'));
        //Primer dia del presente mes
        $fechaFinPeriodo_2 = date("Y-m-d", strtotime($ultimoMes.'-01'));

        //Fecha de incio de ventas periodo -3
        $fechaInicioPeriodo_3 = date("Y-m-d", strtotime($periodo_3.'-01'));
        //Primer dia del presente mes
        $fechaFinPeriodo_3 = date("Y-m-d", strtotime($periodo_2.'-01'));

        //Obtener todos los empleados
        $empleados = Empleado::all();
        $matrisVentasComiBono = $empleados->toArray();

        //Poblar la matriz de empleados con comisiones y bonos
        foreach ($matrisVentasComiBono as $key => $empleado) {
            $matrisVentasComiBono[$key]['ventas_mensual'] = $this->obtenerVentasSum([$empleado['id']], [$fechaInicioPeriodo, $fechaFinPeriodo]);
            $matrisVentasComiBono[$key]['ventas_primera_quincena'] = $this->obtenerVentasSum([$empleado['id']], [$fechaInicioPeriodo, $fechaFinPrimeraQuincena]);
            $matrisVentasComiBono[$key]['comisiones_ventas_propias'] = $this->obtenerComisionesVentasPropias($empleado['id']);
            $matrisVentasComiBono[$key]['comisiones_totales'] = $this->obtenerComisionesVentasTotal($empleado['id']);
            $matrisVentasComiBono[$key]['comisiones_lider'] = $matrisVentasComiBono[$key]['comisiones_totales'] - $matrisVentasComiBono[$key]['comisiones_ventas_propias'];
            //VENTAS DEL EQUIPO DEL PERIODO -1 SIN LIDER
            $matrisVentasComiBono[$key]['ventas_equipo_sin_lider_per-1'] = $this->obtenerVentasEquipo($empleado['id'], false, [$fechaInicioPeriodo, $fechaFinPeriodo]);
            $matrisVentasComiBono[$key]['bono_venta_propia'] = $this->bonoVentaPropia($matrisVentasComiBono[$key]['ventas_mensual']);
            $matrisVentasComiBono[$key]['bono_quincenal'] = $this->bonoQuincenal($matrisVentasComiBono[$key]['ventas_primera_quincena']);
            //VENTAS DE TODO EL EQUIPO DEL PERIODO -1
            $matrisVentasComiBono[$key]['ventas_equipo_con_lider-1'] = $this->obtenerVentasEquipo($empleado['id'], true, [$fechaInicioPeriodo, $fechaFinPeriodo]);
            $matrisVentasComiBono[$key]['bono_liderazgo'] = $this->bonoLiderazgo($matrisVentasComiBono[$key]['ventas_equipo_con_lider-1']);
            //VENTAS DE TODO EL EQUIPO DEL PERIODO -2
            $matrisVentasComiBono[$key]['ventas_equipo_con_lider-2'] = $this->obtenerVentasEquipo($empleado['id'], true, [$fechaInicioPeriodo_2, $fechaFinPeriodo_2]);
            //VENTAS DE TODO EL EQUIPO DEL PERIODO -3
            $matrisVentasComiBono[$key]['ventas_equipo_con_lider-3'] = $this->obtenerVentasEquipo($empleado['id'], true, [$fechaInicioPeriodo_3, $fechaFinPeriodo_3]);
            $matrisVentasComiBono[$key]['bono_trimestral'] = $this->bonoTrimestral([$matrisVentasComiBono[$key]['ventas_equipo_con_lider-1'], $matrisVentasComiBono[$key]['ventas_equipo_con_lider-2'], $matrisVentasComiBono[$key]['ventas_equipo_con_lider-3']]);

            //obtener nivel de empleado
            $arbolJerarquias = $this->obtenerEquipo($empleado['id']);
            $nivelEmpleado = ($this->nivelEmpleado($arbolJerarquias)-1)/2;

            $matrisVentasComiBono[$key]['nivel_empleado'] = $nivelEmpleado;
            $matrisVentasComiBono[$key]['aplica_bono_comisiones_equipo'] = $this->aplicaComisionesBonoEquipo($nivelEmpleado, $matrisVentasComiBono[$key]['ventas_mensual'], $matrisVentasComiBono[$key]['ventas_equipo_sin_lider_per-1']) ? 'SI' : 'NO';
        }

        dd($matrisVentasComiBono);

        return view('reporteventascomisiones');
    }

    /**
     * Método que obtiene todas las ventas a partir de un id de empleado y un periodo de ventas
     *
     * @return decimal monto de las ventas en el periodo
     */
    private function obtenerVentasSum($idEmpleados = [], $rangoFechas = [])
    {
        $montoVenta = Venta::whereIn('idempleado', $idEmpleados);
        if(count($rangoFechas) > 0){
            $montoVenta = $montoVenta->whereBetween('fecha', $rangoFechas);
        }
        return round($montoVenta->sum('monto'), 2);
    }

    private function obtenerComisionesVentasPropias($idEmpleado)
    {
        $arrIdsVentasPropias = Venta::where('idempleado', $idEmpleado)->pluck('id')->toArray();
        $comisionesPropias = Comision::whereIn('idventa', $arrIdsVentasPropias)->where('idempleado', $idEmpleado)->sum('monto');
        return round($comisionesPropias, 2);
    }


    private function obtenerComisionesVentasTotal($idEmpleado)
    {
        $comisionesPropias = Comision::where('idempleado', $idEmpleado)->sum('monto');
        return round($comisionesPropias, 2);
    }


    /**
     *  Dado un id de empleado se obtiene el equipo dependiente (en caso de que aplique) y finalmente se extrae las ventas de todo el equipo
     *
     * @param int $idLider
     * @return decimal monto de venta de equipo
     */
    private function obtenerVentasEquipo($idLider, $conLider = true, $rangoFechas = []){

        //Se obtiene la estructura de arbol de todo el equipo dado el id del lider del equipo
        $equipo = $this->obtenerEquipo($idLider);
        //se convierte el arbol de equipo a un arbol plano
        $equipoUnidimensional = $this->normalizarArrayMultidimensional($equipo);
        if(!$conLider) {
            $equipoFiltrado = $this->filtrarLiderEquipo($equipoUnidimensional, $idLider);
        } else {
            $equipoFiltrado = $equipoUnidimensional; 
        }

        $ventasEquipo = $this->obtenerVentasSum(array_column($equipoFiltrado, 'id'), $rangoFechas);

        return $ventasEquipo;
    }


    private function obtenerEquipo($idEmpleado = null)
    {
        $vendedores = Empleado::with('allChildrenAccounts');
        if (is_null($idEmpleado)) {
            $vendedores = $vendedores->whereNull('idlider');
        } else {
            $vendedores = $vendedores->where('id', $idEmpleado);
        }

        return $vendedores->get()->toArray();
    }

    /**
     * Convierte el array multidimensional (tipo arbol) en un array plano de una dimension
     *
     * @param [type] $array
     * @return void
     */
    private function normalizarArrayMultidimensional($array)
    {
        $result = [];
        foreach ($array as $item) {
            if (is_array($item)) {
                $result[] = array_filter($item, function ($array) {
                    return ! is_array($array);
                });
                $result = array_merge($result, $this->normalizarArrayMultidimensional($item));
            }
        }
        return array_filter($result);
    }


    private function filtrarLiderEquipo($equipoUnidimensional, $idEmpleado)
    {
        //Obtener los miembros del equipo sin lider
        $equipoUnidimensionalFiltrado = [];
        foreach ($equipoUnidimensional as $k => $empleado) {
            if ($empleado['id'] != $idEmpleado) {
                $equipoUnidimensionalFiltrado[] = $empleado;
            }
        }
        return $equipoUnidimensionalFiltrado;
    }

    /**
     * Undocumented function
     *
     * Si la persona vende $5,000 recibe $150
     * Si la persona vende $10,000 recibe $200  
     * Si la persona vende $15,000 recibe $300  
     * Si la persona vende $25,000 recibe $400 
     * 
     * @param [type] $monto
     * @return void
     */
    private function bonoVentaPropia($monto){
        $bono = 0;
        if($monto >= 5000 && $monto < 10000){
            $bono = 150;
        } elseif ($monto >= 10000  && $monto < 15000) {
            $bono = 200;
        } elseif ($monto >= 15000 && $monto <25000) {
            $bono = 300;
        } elseif ($monto >= 25000) {
            $bono = 400;
        }
        return round($bono, 2);
    }

    /**
     *
     * Si la persona vende $5,000 o más en los primeros quince días del mes recibe $100
     * 
     * @param decimal $monto
     * @return decimal bono
     */
    private function bonoQuincenal($monto = 0){
        $bono = 0;
        if($monto >= 5000){
            $bono = 100;
        }
        return round($bono, 2);
    }


    /**
     * Si el equipo, incluyendo al líder vende $20,000 en el mes recibe $150
     * Si el equipo, incluyendo al líder vende $40,000 en el mes recibe $250
     * Si el equipo, incluyendo al líder vende $60,000 en el mes recibe $350
     * Si el equipo, incluyendo al líder vende $80,000 en el mes recibe $500
     *
     * @param decimal $monto
     * 
     * @return decimal bono
     */
    private function bonoLiderazgo($monto = 0){
        $bono = 0;
        if($monto >= 20000 && $monto < 40000) {
            $bono = 150;
        } elseif($monto >= 40000 && $monto < 60000) {
            $bono = 250;
        } elseif($monto >= 60000 && $monto < 80000) {
            $bono = 350;
        } elseif($monto >= 80000) {
            $bono = 500;
        }

        return round($bono, 2);
    }

    /**
     * 
     * Calcula el bono trimestral:
     * Si el equipo, incluyendo al líder vende $45,000 o más por tres meses consecutivos recibe $350
     * Si el equipo, incluyendo al líder vende $65,000 o más por tres meses consecutivos recibe $500
     * Si el equipo, incluyendo al líder vende $85,000 o más por tres meses consecutivos recibe $650
     * Si el equipo, incluyendo al líder vende $100,000 o más por tres meses consecutivos recibe $1000   
     *
     * @param array $ventasPeriodos
     * @return decimal $bono
     */
    private function bonoTrimestral($ventasPeriodos = []){
        $ventaMin = min($ventasPeriodos);
        $bono = 0;

        if($ventaMin >= 45000 && $ventaMin < 65000){
            $bono = 350;
        } elseif($ventaMin >= 65000 && $ventaMin < 85000) {
            $bono = 500;
        } elseif($ventaMin >= 85000 && $ventaMin < 100000) {
            $bono = 650;
        } elseif($ventaMin >= 100000) {
            $bono = 1000;
        }
        return round($bono, 2);
    }

    /**
     * 
     * Determina la prfundidad de una array multidimensional dado
     * 
     * 
     */
    private function nivelEmpleado(array $array) {
        $max_depth = 1;
    
        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = $this->nivelEmpleado($value) + 1;
    
                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }
    
        return $max_depth;
    }

    /**
     * Determina si el lider aplica para los bonos y comisiones
     *
     * @param int $nivel
     * @param decimal $ventaPropia
     * @param decimal $ventaEquipo
     * @return bolean $aplica
     */
    private function aplicaComisionesBonoEquipo($nivel, $ventaPropia, $ventaEquipo){
        $aplica = false;

        switch ($nivel) {
            //No existe ninguna restricción para la comisión del 3% por ventas propias
            case 1:
                $aplica = true;
                break;
            //Para el líder N2 (que tiene un nivel a su cargo): venta propia > $5,000, venta del equipo > $35,000
            case 2:
                if($ventaPropia > 5000 && $ventaEquipo > 35000){
                    $aplica = true;
                }
                break;
            //Para el líder N3 (que tiene dos niveles a su cargo): venta propia > $8,000, venta del equipo > $60,000, numero de lideres n2 en su equipo >= 1
            case 3:
                if($ventaPropia > 8500 && $ventaEquipo > 60000){
                    $aplica = true;
                }
                break;
            //Para el líder N4 (que tiene tres niveles a su cargo): venta propia > $8,000, venta del equipo > $120,000
            case 4:
                if($ventaPropia > 8000 && $ventaEquipo > 120000){
                    $aplica = true;
                }
                break;
            //Para el líder N5 (que tiene cuatro niveles a su cargo): venta propia > $6,000, venta del equipo > $240,000
            case 5:
                if($ventaPropia > 6000 && $ventaEquipo > 240000){
                    $aplica = true;
                }
                break;
        }
        return $aplica;
    }
}