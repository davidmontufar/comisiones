<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table = 'dbo.empleados';
    protected $connection = 'sqlsrv';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $keyType = 'int';


    public function childrenAccounts()
    {
        return $this->hasMany($this, 'idlider', 'id');
    }
    
    public function allChildrenAccounts()
    {
        return $this->childrenAccounts()->with('allChildrenAccounts');
    }

    public function parent(){
        return $this->hasOne( $this, 'id', 'idlider' );
      }
}
