<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comision extends Model
{
    protected $table = 'dbo.comisiones';
    protected $connection = 'sqlsrv';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $keyType = 'int';

    protected $fillable = [
        'monto',
        'idempleado',
        'idventa',
        'descripcion'
    ];
}
