<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ComisionesController@index')->name('index');
Route::get('regventafrm', 'ComisionesController@desplegarRegistrarVenta')->name('show_reg_venta');
Route::post('registrarventa', 'ComisionesController@registrarVenta')->name('registrar_venta');

/* Reporte y calculo de ventas comisiones y bonos */
Route::get('venttascomibonosfrm', 'ComisionesController@desplegarReporteVentComBon')->name('show_reporte');



